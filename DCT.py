from pyexcel_ods import get_data
import sys

PAGE_START_ADDRESS  = "0x0801F800"

OdsInputFileName = "NVM.ods"
HexOutputFileName = "output.hex"

helpMessage = "\n**************************************************\n\n"
helpMessage+= "\t-o specify .hex output file name\n"
helpMessage+= "\t-i specify .ods input file containing NVM data\n"
helpMessage+= "\t-a specify Page Start Address format 0x00000000\n\n"
helpMessage+= "**************************************************"
helpFlag = 0

#****** sys.argv arguments evaluation ********
try:
    for i in range(1, len(sys.argv), 2):
        if(sys.argv[i] == "-i"):
            OdsInputFileName = sys.argv[i+1]
                
        if(sys.argv[i] == "-o"):
            HexOutputFileName = sys.argv[i+1]
            
        if(sys.argv[i] == "-a"):
            PAGE_START_ADDRESS = sys.argv[i+1]
            
        if((sys.argv[i] == "-help") or (sys.argv[i] == "-h")):
            helpFlag = 1
            print(helpMessage)
except:
    print("Wrong sys.argv")
#*********************************************
    

#******* End message creation ****************
endMessage = "Intel " 
endMessage += HexOutputFileName
endMessage += " file created correctly"
#*********************************************


CRC_COMPLEMENT_NUMBER   = 256
END_OF_FILE             =":00000001FF"

#zbuduj enum do wklejenia
def buildNvmEnumList(lNazw): 
    
    file = open("NVM_EntryList.h", "w")
    
    print("typedef enum {")
    file.write("typedef enum {\n")
    for a in lNazw:
        print("\t",a[1]," = ", a[0])
        file.write("\t" + str(a[1]) + " = " + str(a[0]) + ",\n")
    print("\tNVM_EntryCount")
    file.write("\tNVM_EntryCount\n} NVM_DataId_e;")
    print("} NVM_DataId_e;")


def buildPreDataLine(adressHex = "0x00000000"):
    
    dataBeginVal = 6
    
    if(adressHex[1] == "x"):
        firstHalf = adressHex.split("x")[1][0:2]
        secondHalf = adressHex.split("x")[1][2:4]
    else:
        firstHalf = adressHex[0:2]
        secondHalf = adressHex[2:4]
    
    
    sumadoCRC = (dataBeginVal + int(firstHalf,16) + int(secondHalf, 16))
    
    if(len(hex(sumadoCRC).split("x")[1])<2):
        crcString = ("0" + hex(sumadoCRC).split("x")[1][-1])
    else:
        crcString = (hex(sumadoCRC).split("x")[1][-2] + hex(sumadoCRC).split("x")[1][-1])
    
    crc = CRC_COMPLEMENT_NUMBER - int(crcString, 16)
    
    returnSt = ":02000004" + firstHalf + secondHalf + hex(crc).split("x")[1]
    
    return returnSt

def buildDataLine(adressHex = "0x00000000", dataString = "0000"):
    
    dataArr = []
    dataSum = 0
    
    if(adressHex[1] == "x"):
        firstHalf = adressHex.split("x")[1][4:6]
        secondHalf = adressHex.split("x")[1][6:8]
    else:
        firstHalf = adressHex[4:6]
        secondHalf = adressHex[6:8]

    for i in range(0, len(dataString), 2):
        dataArr.append(dataString[i]+dataString[i+1])
    
    for i in dataArr:
        dataSum += int(i, 16)
    
    sumadoCRC = (len(dataArr) + int(firstHalf, 16) + int(secondHalf, 16) + dataSum)
    
    if(len(hex(sumadoCRC).split("x")[1])<2):
        crcString = ("0" + hex(sumadoCRC).split("x")[1][-1])
    else:
        crcString = (hex(sumadoCRC).split("x")[1][-2] + hex(sumadoCRC).split("x")[1][-1])
    
    crc = CRC_COMPLEMENT_NUMBER - int(crcString, 16)
    
    #print(hex(len(dataArr)).split("x")[1])
    
    returnSt = ":" + normalizHexValue((hex(len(dataArr)).split("x")[1]),2) + firstHalf + secondHalf + "00" + dataString + hex(crc).split("x")[1]
    
    return returnSt

def buildHexFile(address = "0x00000000", dataLarray = []):
    
    hexFile = open(HexOutputFileName, "w")
   
    lineAddr = int(address, 16)
    
    preDataLine     = buildPreDataLine(normalizHexValue(hex(lineAddr), 8))
    hexFile.write(preDataLine + "\n")
    
    for entry in dataLarray:
        if(len(entry) > 0):
            dataLine = buildDataLine(normalizHexValue(hex(lineAddr), 8), entry)
            hexFile.write(dataLine + "\n")
        lineAddr += 16
        
    hexFile.write(END_OF_FILE)
    
    hexFile.close()
    
def applyByteEndianess(stringNumber):
    retStr = ""
    
    for i in range((len(stringNumber)-2), -1, -2):
        retStr += stringNumber[i] + stringNumber[i+1]
    
    return retStr


def normalizHexValue(hexString, lenghth):
    retStr = ""
    
    if(len(hexString) > 2) and (hexString[1] == "x"):
        hexNumber = hexString
    else:
        hexNumber = "0x" + hexString
        
    
    if(len(hexNumber.split("x")[1]) < lenghth):
        actLen = len(hexNumber.split("x")[1])
        retStr = ("0" * (lenghth - actLen)) + hexNumber.split("x")[1]
    else:
        retStr = hexNumber.split("x")[1]
    
    if(lenghth == 4):
        #return retStr
        return applyByteEndianess(retStr)
    else:
        return retStr


def buildDataLines(valuesList):
    dataLines = []
    
    licznik = 0
    
    dataLines.append("")
    for i in range(len(valuesList)):
        
        valStr = (normalizHexValue(hex(valuesList[i][0]), 2) + normalizHexValue(hex(valuesList[i][1]), 2) + normalizHexValue(hex(valuesList[i][2]), 4))
        dataLines[licznik] += valStr
        
        if((i+1) - (4*licznik) == 4):
            licznik +=1
            dataLines.append("")
            
    return dataLines

def getItemsFromFile(fileName = "NVM.ods"):
    itemsArr = []
    
    #read data from file
    data = get_data(fileName)
    
    #find all rows od NVM entities and pack them into an array
    for a,b in data.items():
        for i in b[1:]:
            itemsArr.append(i)
    
    return itemsArr

def getFinalData(dataLinesArr):
    nameList    = []
    valuesList  = []
    
    #build first entry for Page header
    valuesList.append([0,0,0])
    
    localCounter = 0
    for i in dataLinesArr:
        
        #data for enum build
        nameList.append([localCounter, i[0]])
        
        #data for .hex file entry
        nvm_Id      = localCounter
        nvm_extnd   = i[1]
        nvmData16   = i[2]
        
        valuesList.append([nvm_Id, nvm_extnd, nvmData16])
        localCounter += 1
        
    return nameList, valuesList

if __name__ == "__main__":
    
    if not helpFlag:
    
        nvmEntryNameList = []
        nvmEntryValuesList = []
        fileData = []
        
        nvmEntryNameList, nvmEntryValuesList = getFinalData(getItemsFromFile(OdsInputFileName))
        
        buildNvmEnumList(nvmEntryNameList)
        
        buildHexFile(PAGE_START_ADDRESS, buildDataLines(nvmEntryValuesList))
    
    print(endMessage)
