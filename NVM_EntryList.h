typedef enum {
	nvm_PierwszyBajt = 0,
	nvm_DwadziesciaTrzy = 1,
	nvm_DwadziesciaCztery = 2,
	nvm_DwadziesciaPiec = 3,
	nvm_DwadziesciaSzesc = 4,
	nvm_DwadziesciaSiedem = 5,
	nvm_DwadziesciaOsiem = 6,
	nvm_SzOsDwiescie = 7,
	NVM_EntryCount
} NVM_DataId_e;